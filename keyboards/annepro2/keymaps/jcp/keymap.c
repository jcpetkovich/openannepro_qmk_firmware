 /* Copyright 2021 OpenAnnePro community
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 2 of the License, or
  * (at your option) any later version.
  *
  * This program is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.
  *
  * You should have received a copy of the GNU General Public License
  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  */

#include QMK_KEYBOARD_H
#include <jcmacros.h>

enum anne_pro_layers {
    _COLEMAK,
    _LOWER,
    _RAISE,
    _ADJUST,
    _GAMING
};

enum anne_pro_keycodes {
    COLEMAK = SAFE_RANGE,
    RAISE,
    LOWER,
    BACKLIT,
    GAMING,
    EXIT
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
 [_COLEMAK] = LAYOUT( /* Base */
     KC_GRV,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,   KC_8,      KC_9,    KC_0,    KC_MINS, KC_EQL, KC_BSPC,
     KC_ESC,  KC_Q,    KC_W,    KC_F,    KC_P,    KC_G,    KC_J,    KC_L,   KC_U,      KC_Y,    KC_SCLN, KC_LBRC, KC_RBRC, KC_BSLS,
     KC_TAB,  KC_A,    KC_R,    KC_S,    KC_T,    KC_D,    KC_H,    KC_N,   KC_E,      KC_I,    KC_O,    KC_QUOT, KC_ENT,
     KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_K,    KC_M,   KC_COMM,   KC_DOT,  SFT_SLS, KC_RSFT,
     KC_LGUI, KC_LALT, LOWER,                     KC_SPC,                  KC_LCTL, RAISE, KC_ENT,  KC_RALT  ),

 [_LOWER] = LAYOUT( /* Base */
     _______, _______, _______, _______, _______, KC_PSCR, _______, _______, _______, _______, KC_PLUS, _______, _______, KC_BSPC,
     _______, DWORD,   BWORD,   KC_UP,   FWORD,   KC_HOME, KC_PGUP, KC_7,    KC_8,    KC_9,    KC_MINS, _______, _______, _______,
     _______, KC_DEL,  KC_LEFT, KC_DOWN, KC_RGHT, KC_END,  KC_PGDN, KC_4,    KC_5,    KC_6,    KC_ASTR, KC_0,    _______,
     _______, _______, CUT,     COPY,    PASTE,   KC_SPC,  KC_COMM, KC_1,    KC_2,    KC_3,    KC_DOT,  _______,
     _______, _______, _______,                    _______,                _______, _______, _______, _______  ),

 [_RAISE] = LAYOUT( /* Base */
     _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,
     _______, KC_EXLM, KC_AT,   KC_HASH, KC_DLR,  KC_PERC, KC_CIRC, KC_AMPR, KC_ASTR, KC_LPRN, KC_RPRN, _______, _______, _______,
     _______, KC_TILD, KC_LBRC, KC_RBRC, KC_LCBR, KC_RCBR, _______, _______, _______, _______, KC_BSLS, _______, _______,
     _______, KC_GRV,  KC_LPRN, KC_RPRN, _______, KC_PIPE, _______, _______, KC_LABK, KC_RABK, _______, _______,
     _______, _______, _______,                    _______,                _______, _______, _______, _______  ),

 [_ADJUST] = LAYOUT( /* Base */
     _______, KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_INS,
     _______, AP2_BT1, AP2_BT2, AP2_BT3, AP2_BT4, RGB_MOD, RGB_HUI, RGB_SAI, RGB_SPI, RGB_VAI, RGB_TOG, _______, _______, _______,
     _______, AU_ON,   AU_OFF,  AG_NORM, AG_SWAP, _______, _______, _______, _______, _______, _______, _______, GAMING,
     _______, MUV_DE,  MUV_IN,  MU_ON,   MU_OFF,  MI_ON,   MI_OFF,  _______, _______, _______, _______, _______,
     _______, _______, _______,                    _______,                _______, _______, _______, _______  ),

 [_GAMING] = LAYOUT( /* Base */
     _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, EXIT,
     _______, KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    EXIT,    _______, _______,
     _______, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    _______, _______, _______,
     _______, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, _______,
     KC_LCTL, KC_1,    KC_2,                       _______,                KC_4,    KC_5,    KC_6,    KC_7     ),

};
// clang-format on

const uint16_t PROGMEM fn_actions[] = {

};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
	switch(keycode) {
	case COLEMAK:
		if (record->event.pressed) {
			set_single_persistent_default_layer(_COLEMAK);
		}
		return false;
		break;
	case LOWER:
		if (record->event.pressed) {
			layer_on(_LOWER);
			update_tri_layer(_LOWER, _RAISE, _ADJUST);
		} else {
			layer_off(_LOWER);
			update_tri_layer(_LOWER, _RAISE, _ADJUST);
		}
		return false;
		break;
	case RAISE:
		if (record->event.pressed) {
			layer_on(_RAISE);
			update_tri_layer(_LOWER, _RAISE, _ADJUST);
		} else {
			layer_off(_RAISE);
			update_tri_layer(_LOWER, _RAISE, _ADJUST);
		}
		return false;
		break;
	case GAMING:
		if (record->event.pressed) {
			set_single_persistent_default_layer(_GAMING);
		}
		return false;
		break;
	case EXIT:
		if (record->event.pressed) {
			set_single_persistent_default_layer(_COLEMAK);
		}
		return false;
		break;
	}
	return true;
};
